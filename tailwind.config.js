module.exports = {
	content: ["./templates/**/*.{html,js,twig}"],
	theme: {
		colors: {
			'dark': {
				'black': "#1C264A",
				'blue': "#222E59",
				'gray': "#324A5F"
			},
			'light': {
				'blue': "#5C7389",
				'skin': "#998287"
			},
			'blood': "#FF4E4B",
			'error': '#FF0000',
			'success': '#00FF00'
		},
	},
	plugins: [],
}